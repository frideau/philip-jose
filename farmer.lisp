;;; -*- Mode: Lisp ; Base: 10 ; Syntax: ANSI-Common-Lisp -*-
;;;;; Main loop for the Philip-Jose Farmer

;;; Manage a farm of processes each doing a part of a grand job.

(in-package :philip-jose)

(defparameter *philip-jose-farmer-version* 0)

(defun-request-handler :farmer-version ()
  (reply *philip-jose-farmer-version*))

(defun-request-handler :square (n)
  (reply (* n n)))

(defun-request-handler :1+ (n)
  (reply (1+ n)))

(defun-request-handler :reload ()
  (reply (asdf:oos 'asdf:load-op *farmer-system*)))

(defun-request-handler :register-client (id &key valid explanation)
  (logger "~&Registering client~{ ~A~}" id)
  (reply t))

(defun-request-handler :armageddon ()
  (flet ((kill-worker-id (worker-id)
           (destructuring-bind (machine process connect-time) worker-id
             (declare (ignorable connect-time))
             (logger "~&Armageddon killing pid ~D on ~A" process machine)
             (kill-machine-process machine process))))
    (maphash (lambda (worker-id status)
               (declare (ignorable status))
               (kill-worker-id worker-id))
             *registered-workers*)
    (maphash (lambda (job-id job)
               (declare (ignorable job-id))
               (kill-worker-id (car (job-status job))))
             *claimed-worker-jobs*))
  (reply t)
  (logger "~&Armageddon quitting")
  (sb-ext:quit))

(defun-job fake (m)
  (setf *random-state* (make-random-state t))
  (let ((x (* 1d-3 (random 10000))))
    (DBG :fake m x)
    (fsleep x)
    (values m x)))


(defun show-tasks ()
  (let ((*print-level* 4))
    (logger "~&scheduled local-tasks: ~S~%" (fifo-head *scheduled-local-tasks*))
    (logger "~&timed local-tasks: ~S~%" (container-contents *timed-local-tasks*))
    (logger "~&scheduled worker jobs: ~S~%" (fifo-head *scheduled-worker-jobs*))
    (logger "~&claimed worker jobs: ~S~%" (hash-table->alist *claimed-worker-jobs*)))
  nil)

(defun clear-tasks ()
  (fifo-empty! *scheduled-local-tasks*)
  (empty-container! *timed-local-tasks*)
  (fifo-empty! *scheduled-worker-jobs*)
  (clrhash *claimed-worker-jobs*)
  nil)

#|
(trace call-request-handler handle-sexp-request)

(hash-table->alist *request-heads*)
|#
