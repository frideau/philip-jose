;;; -*- Mode: Lisp ; Base: 10 ; Syntax: ANSI-Common-Lisp -*-

(in-package :cl-user)

(cl:defpackage :philip-jose
  (:use :common-lisp
        :fare-utils
        :split-sequence :parse-number
        :net.sockets :cffi :iomux
        :threads
        :arnesi :closer-mop)
  (:shadowing-import-from :arnesi
      #:aif2 #:acond2 #:aif #:acond #:it ;same anaphoric macros in fare-utils
      #:let1 #:compose #:if-bind ;also in fare-utils
      #:join-strings #:copy-array #:strcat #:quit ;also in fare-utils
      #:_ ;conflicts with fare-utils
      #:name ;conflicts with net.sockets
      #:partition ;conflicts with split-sequence
      )
  (:export #:*farmer-path* #:*farmer-system* #:*id*
           #:*local-task-name* #:*max-request-size*
           #:*parallel-job-issuer* #:*registry-path*
           #:*retry-on-timeout* #:*ssh-path* #:*startup-actions*
           #:*usage* #:*workers-per-machine* #:+crlf+ #:+unix-epoch+
           #:bork #:call-job-handler #:call-request-handler
           #:call-with-parallel-job-issuer #:collect-slots
           #:command-line-from-strings #:defun-job
           #:defun-request-handler #:errexit #:errformat #:exit #:fsleep
           #:funkall #:get-localhost-names #:get-real-time #:getarg
           #:gethostname #:issue-parallel-job #:issue-sequential-job
           #:issue-sequential-job-with-timeout #:issue-worker-job/cc
           #:logger #:main #:no-more-arg #:no-more-args #:print-object
           #:program-help #:register-job-handler #:register-request-head
           #:reply #:reply* #:simple-client #:simple-print-object
           #:simple-print-object-mixin #:simple-worker-loop
           #:simple-worker-thing #:slots-to-print #:spawn-all-clients
           #:spawn-process #:spawn-process-on #:spawn-remote-process
           #:start-clients #:start-multi-threaded-server
           #:start-single-threaded-server #:start-test #:startup
           #:stop-server #:strings-from-command-line
           #:strings-from-command-line-stream #:task-loop #:unexpected-end-of-text
           #:with-error-message-handler #:with-parallel-jobs
           #:with-rendez-vous #:with-rendez-vous-after
           #:with-trivial-logging))

;;(write (fare-utils:make-defpackage-form :philip-jose :gensym) :pretty t :case :downcase)(terpri)
