;;; -*- Mode: Lisp ; Base: 10 ; Syntax: ANSI-Common-Lisp -*-
;;;; Trivial TCP client

(in-package :philip-jose)

(defun make-tcp-connection (address port)
  (let ((socket (make-socket :address-family :internet :type :stream :connect :active
                             :protocol :default :ipv6 nil)))
    (connect socket address :port port)
    socket))

(defun ensure-address (x)
  (etypecase x
    (sockaddr x)
    (string
     (let ((host (lookup-host x :ipv6 nil)))
       (first (host-addresses host))))))


(def*fun simple-client (message target port)
  (let* ((address (ensure-address target))
         (socket (make-tcp-connection address port)))
    (unwind-protect
         (progn
           (safe-write message :stream socket)
           (princ +crlf+ socket)
           (finish-output socket)
           ;;(shutdown socket :write)
           (read socket))
      (close socket))))


