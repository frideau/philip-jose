FARMER_PATH=/home/fare/off-hours-computations/lisp/
LISP_SYSTEMS=/home/fare/lib/lisp-systems/
SYSTEM=philip-jose
LISP=sbcl

farmer.sh: setup.lisp
	cl-launch -p ${LISP_SYSTEMS} -p ${FARMER_PATH} -l ${LISP} -f $< -s ${SYSTEM} -r philip-jose::farmer-start -o $@
