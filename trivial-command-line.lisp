;;; -*- Mode: Lisp ; Base: 10 ; Syntax: ANSI-Common-Lisp -*-
;;;;; Trivial Command-Line server.

;; This provides a simple command-line dispatch framework,
;; where each set of arguments 
;; a list of non-whitespace words separated by whitespace (duh!)
;; where the first word is the operator and other words are arguments.

(in-package :philip-jose)

;;; A bit of parsing and unparsing between shell command-lines and list of shell strings
(defmacro define-ascii-char-code-predicates (name (x) &body body)
  ;; assumes ASCII / latin1 / unicode (!)
  (let* ((code-p-name (conc-symbol "ASCII-" name "-CODE-P"))
         (char-p-name (conc-symbol "ASCII-" name "-CHAR-P")))
    `(progn
      (defun ,code-p-name (,x) ,@body)
      (defun ,char-p-name (c) (,code-p-name (char-code c))))))
(define-ascii-char-code-predicates uppercase-letter (x)
  (<= 65 x 90))
(define-ascii-char-code-predicates lowercase-letter (x)
  (<= 97 x 122))
(define-ascii-char-code-predicates digit (x)
  (let ((d (- x #x30)))
    (when (<= 0 d 9)
      d)))
(define-ascii-char-code-predicates whitespace (x)
    (or (<= x 32)
        (= x 127)
        (= x #xA0)))
(define-ascii-char-code-predicates letter (x)
  (or (ascii-uppercase-letter-code-p x)
      (ascii-lowercase-letter-code-p x)))
(define-ascii-char-code-predicates letter-or-digit (x)
  (or (ascii-letter-code-p x)
      (ascii-digit-code-p x)))
(define-ascii-char-code-predicates normal-shell (x)
  (or (ascii-letter-code-p x)
      (<= #x2B x #x3A) ; +,-./0123456789:
      (member x '(#x25 #x3D #x5F)))) ; %=_

(defun normal-shell-string-p (s)
  (and (stringp s)
       (every #'ascii-normal-shell-char-p s)))

(defun write-string-single-quoted-stream (string stream)
  (write-char #\' stream)
  (loop for c across string do
        (case c
          ((#\' #\\) (write-char #\\ stream)))
        (write-char c stream))
  (write-char #\' stream))

(def*fun command-line-from-strings (strings &optional stream)
  (with-output (stream)
    (loop for (s . rest) on strings do
          (if (normal-shell-string-p s)
            (write-string s stream)
            (write-string-single-quoted-stream s stream))
          (when rest (write-char #\space stream)))))

(def*fun strings-from-command-line (command-line)
  (with-input-from-string (s command-line)
    (strings-from-command-line-stream s)))

(define*-condition unexpected-end-of-text (simple-error) ())

(def*fun strings-from-command-line-stream (command-line-stream)
  ;; handles quoting and escaping like a shell, but does not handle $ and other subtleties
  (declare (optimize (speed 3) (safety 2) (debug 1)))
  (let* ((s (make-string-output-stream))
         (r nil))
    (labels
        ((flush ()
           (push (get-output-stream-string s) r))
         (exit ()
           (return-from strings-from-command-line-stream (nreverse r)))
         (putc (c)
           (write-char c s))
         (getc ()
           (read-char command-line-stream nil))
         (ungetc (c)
           (unread-char c command-line-stream))
         (eot (x &rest args)
           (error 'unexpected-end-of-text
                  :format-control x
                  :format-arguments args))
         (skip-spaces ()
           (loop for c = (getc)
                 while (and c (ascii-whitespace-char-p c))
                 finally
                 (if c
                   (ungetc c)
                   (exit)))
           (get-normal-char))
         (get-normal-char ()
           (let ((c (getc)))
             (case c
               ((nil) (flush) (exit))
               (#\\ (get-escaped-char))
               (#\' (get-single-quoted-char))
               (#\" (get-double-quoted-char))
               (otherwise
                (if
                  (ascii-whitespace-char-p c)
                  (progn (flush) (skip-spaces))
                  (progn (putc c) (get-normal-char)))))))
         (get-escaped-char ()
           (let ((c (getc)))
             (case c
               ((nil) (eot "Command-line ended where escaped character expected"))
               (otherwise (putc c) (get-normal-char)))))
         (get-double-quoted-char ()
           (let ((c (getc)))
             (case c
               ((nil) (eot "Command-line ended inside double quotes"))
               (#\\ (get-escaped-double-quoted-char))
               (#\" (get-normal-char))
               (otherwise (putc c) (get-double-quoted-char)))))
         (get-escaped-double-quoted-char ()
           (let ((c (getc)))
             (case c
               ((nil) (eot "Command-line ended where escaped double quoted character expected"))
               (otherwise (putc c) (get-double-quoted-char)))))
         (get-single-quoted-char ()
           (let ((c (getc)))
             (case c
               ((nil) (eot "Command-line ended inside single quotes"))
               (#\\ (get-escaped-single-quoted-char))
               (#\' (get-normal-char))
               (otherwise (putc c) (get-single-quoted-char)))))
         (get-escaped-single-quoted-char ()
           (let ((c (getc)))
             (case c
               ((nil) (eot "Command-line ended where escaped character expected"))
               ((#\\ #\') (putc c) (get-single-quoted-char))
               (otherwise (putc #\\) (putc c) (get-single-quoted-char))))))
      (skip-spaces))))

(def*var *usage*
  "farmer [action] [args...]

For a list of valid actions, try
  farmer help
")

(def*parameter *startup-actions*
  '(("server" start-server "[port]") ; the main server tracks what happens
    ("client" start-client "server port"); each client is a worker (future plan: spawns and monitors workers)
    ;;("worker" start-worker "server port id job") ; each worker does a minutes worth of work
    ("test" start-test) ; test
    ("status" query-status) ; queries the current status
    ("help" program-help) ; queries available actions
    (nil start-server "server"))) ; default: start the server

(def*fun bork (&rest r)
  (declare (ignore r))
  (error "bork"))

(def*fun exit (&optional (code 0) &rest r)
  (declare (ignore r))
  (throw :exit code))

(def*fun errformat (fmt &rest args)
  (apply #'format *error-output* fmt args))

(def*fun errexit (code fmt &rest args)
  (apply #'errformat fmt args)
  (exit code))

(defun call-argument-getter (x decoder-function default-thunk errmsg)
  (handler-case
      (if x
        (funcall decoder-function x)
        (funcall default-thunk))
    (t () (errexit 3 "~&Invalid argument~@[ ~A~]~%~A" errmsg *usage*))))

(def*macro getarg (args decoder &optional default errmsg)
  `(call-argument-getter
    (pop ,args)
    (function ,decoder)
    #'(lambda () ,default)
    ,errmsg))

(def*macro no-more-args (args)
  `(getarg ,args bork nil "(too many arguments)"))

(def*fun start-test (&rest r)
  (let ((foo (getarg r parse-integer 0)))
    (no-more-args r)
    (DBG :test
         foo
         (gethostname)
         (process-registry-log))))

(def*fun program-help (&rest r)
  (let ((s (if (streamp (car r)) (car r) *error-output*)))
  (format s "~&Available actions:~%")
  (loop with d = nil for x in *startup-actions* do
        (if (car x)
          (format s "   ~{~A~*~#[~; ~A~]~}~%" x)
          (setf d (third x)))
        finally (when d (format s "default action: ~A~%" d)))))

(defun start-server (&rest r)
  (let* ((port (getarg r parse-integer 6666)))
    (no-more-args r)
    (start-single-threaded-server :client-starter #'start-clients)
    (task-loop)))

(defun start-client (&rest r)
  (let* ((server (getarg r identity (bork)))
         (port (getarg r parse-integer (bork))))
    (no-more-args r)
    (validate-and-start-client server port)))

(defun start-worker (&rest r)
  (let* ((server (getarg r identity (bork)))
         (port (getarg r parse-integer (bork)))
         (id (getarg r read-from-string (bork)))
         (job (getarg r read-from-string (bork))))
    (no-more-args r)
    ;;---***
    (DBG :start-worker server port id job)))

(defun query-status (&rest r)
  (no-more-args r)
  ;;---***
  (DBG :query-status))

(def*fun main (args)
  (let* ((action (pop args))
         (fun (second (assoc action *startup-actions* :test #'equalp))))
    (catch :exit
      (if fun
        (progn (apply fun args) 0)
        (errexit 2 "~&Invalid action ~S~%~A" action *usage*)))))

(def*fun startup ()
  (cl-launch:quit (main cl-launch:*arguments*)))
