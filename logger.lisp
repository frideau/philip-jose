;;; -*- Mode: Lisp ; Base: 10 ; Syntax: ANSI-Common-Lisp -*-
;;;;; Trivial Logging mechanism

(in-package :philip-jose)

(defun trivial-logger (s fmt &rest args)
  (with-standard-io-syntax
    (let ((s (or s *error-output*))
          (*print-pretty* nil)
          (*print-readably* nil))
      (format s "~&")
      (apply #'format s fmt args)
      (format s "~&")
      (finish-output s))))

(def*fun logger (fmt &rest args)
  (apply #'trivial-logger *error-output* fmt args))

(def*macro with-trivial-logging ((&optional s) &body body)
  `(let ((*error-output* (or ,s *error-output*)))
    (handler-bind
        ((condition #'(lambda (condition)
                        (logger "~&~S~%" condition)
                        #+sbcl (ignore-errors (sb-debug:backtrace 100)))))
      ,@body)))
