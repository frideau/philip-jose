;;; -*- Mode: Lisp ; Base: 10 ; Syntax: ANSI-Common-Lisp -*-
;;;;; Default configuration for the Philip-Jose Farmer

(in-package :philip-jose)

(def*var *farmer-system*
    :farmer
  "Name of the system, as visible to the user. Override with the name of your application")

(def*var *farmer-path*
    "/home/fare/off-hours-computations/bin/farmer"
  "path of the farmer, to spawn new clients with fork+exec or ssh")

(def*var *registry-path*
    "/home/fare/off-hours-computations/incoming/log"
  "path of the registry identifying which machines are part of the farm")

(def*var *ssh-path*
    "/usr/bin/ssh"
  "Path to the SSH executable, for fork+exec")

(def*parameter +crlf+
    (make-array
     '(2) :element-type 'base-char
     :initial-contents (list (code-char 13) (code-char 10)))
  "Constant string for CR+LF, the standard line terminator on the net")

(def*var *id*
    nil
  "Identity of the current process: a list with machine name, pid and start time")

(def*var *local-task-name*
    nil
  "TODO: use this to give a name to the currently executed task, for debugging purpose")

(def*var *max-request-size*
    4096  ;; for debugging, use 128. Normally, more like 4096
  "Maximum size of a request, in bytes")
;; Note that (1) the response can be of any size, and
;; (2) at present, we preallocate a buffer for the whole request
;; instead of making it extensible so don't make it too large without fixing the code.

(def*var *workers-per-machine*
    4
  "Number of workers to spawn per machine")
;; TODO: have a dynamic strategy that takes the number of CPUs and/or the load into account.

(defparameter *sleep-delay*
  10
  "duration in seconds to ask workers to sleep when there is no job ready for them yet")


;;; Global variables for server stuff
(defparameter *server* nil)
(defparameter *port* 6666)
(defparameter *server-event* nil)

(defparameter *event-base* nil
  "Base object for I/O multiplexing")
